package buu.nattiya.plus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.nattiya.plus.databinding.FragmentPlusBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    private lateinit var binding: FragmentPlusBinding
    private lateinit var plusViewModel: PlusViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plus, container, false)
        plusViewModel = ViewModelProvider(this).get(PlusViewModel::class.java)
        binding.plusViewModel = plusViewModel

        plusViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                enableAllButton()
                binding.invalidateAll()
                plusViewModel.onNextFinish()
            }

        })

        plusViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                view?.findNavController()?.navigate(R.id.action_fragmentPlus_to_fragmentStartmenu)
                plusViewModel.changeToHomeFinish()
            }

        })

        plusViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                plusViewModel.onSelectChoice1Finish()
            }

        })

        plusViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                plusViewModel.onSelectChoice2Finish()
            }

        })

        plusViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                plusViewModel.onSelectChoice3Finish()
            }

        })
        return binding.root
    }

    fun disabledAllButton() {
        binding.apply {
            btn1.isEnabled = false
            btn2.isEnabled = false
            btn3.isEnabled = false
        }
    }

    fun enableAllButton() {
        binding.apply {
            btn1.isEnabled = true
            btn2.isEnabled = true
            btn3.isEnabled = true
        }
    }

}
package buu.nattiya.plus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.nattiya.plus.databinding.FragmentMinusBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MinusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusFragment : Fragment() {
    private lateinit var binding: FragmentMinusBinding
    private lateinit var minusViewModel: MinusViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_minus, container, false)
        minusViewModel = ViewModelProvider(this).get(MinusViewModel::class.java)
        binding.minusViewModel = minusViewModel

        minusViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                enableAllButton()
                binding.invalidateAll()
                minusViewModel.onNextFinish()
            }

        })

        minusViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                view?.findNavController()?.navigate(R.id.action_fragmentMinus_to_fragmentStartmenu)
                minusViewModel.changeToHomeFinish()
            }

        })

        minusViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                minusViewModel.onSelectChoice1Finish()
            }

        })

        minusViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                minusViewModel.onSelectChoice2Finish()
            }

        })

        minusViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                minusViewModel.onSelectChoice3Finish()
            }

        })
        return binding.root
    }

    fun disabledAllButton() {
        binding.apply {
            btn1.isEnabled = false
            btn2.isEnabled = false
            btn3.isEnabled = false
        }
    }

    fun enableAllButton() {
        binding.apply {
            btn1.isEnabled = true
            btn2.isEnabled = true
            btn3.isEnabled = true
        }
    }
}
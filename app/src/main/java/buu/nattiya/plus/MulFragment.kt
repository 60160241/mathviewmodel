package buu.nattiya.plus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.nattiya.plus.databinding.FragmentMulBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MulFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MulFragment : Fragment() {
    private lateinit var binding: FragmentMulBinding
    private lateinit var mulViewModel: MulViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_mul, container, false)
        mulViewModel = ViewModelProvider(this).get(MulViewModel::class.java)
        binding.mulViewModel = mulViewModel

        mulViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                enableAllButton()
                binding.invalidateAll()
                mulViewModel.onNextFinish()
            }

        })

        mulViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                view?.findNavController()?.navigate(R.id.action_fragmentMul_to_fragmentStartmenu)
                mulViewModel.changeToHomeFinish()
            }

        })

        mulViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                mulViewModel.onSelectChoice1Finish()
            }

        })

        mulViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                mulViewModel.onSelectChoice2Finish()
            }

        })

        mulViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if (hasNext) {
                disabledAllButton()
                binding.invalidateAll()
                mulViewModel.onSelectChoice3Finish()
            }

        })
        return binding.root
    }

    fun disabledAllButton() {
        binding.apply {
            btn1.isEnabled = false
            btn2.isEnabled = false
            btn3.isEnabled = false
        }
    }

    fun enableAllButton() {
        binding.apply {
            btn1.isEnabled = true
            btn2.isEnabled = true
            btn3.isEnabled = true
        }
    }

}